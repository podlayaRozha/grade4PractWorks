<?php
include "fast_sort.php";

function arifm_subsract()
{
	$count=100000;
	$var=100001;
	$startTime=microtime(1);
	for ($i=0; $i<$count; $i++) {
		$var--;
	}
	return round((microtime(1)-$startTime)*1000, 3);
}

function substring()
{
	$count=100000;
	$var=100001;
	$startTime=microtime(1);
	for ($i=0; $i<$count; $i++) {
		strripos("someString", "String");
	}
	return round((microtime(1)-$startTime)*1000, 3);
}

function readWriteFile()
{
	$count=100000;
	$var=100001;
	$startTime=microtime(1);
	for ($i=0; $i<$count; $i++) {
		$input = file_get_contents("input.txt");
		file_put_contents("output.txt", $input);
	}
	return round((microtime(1)-$startTime)*1000, 3);
}

function readFromDB()
{
	$link = mysqli_connect( 
            'localhost',  
            'root',       
            '',   
            'test');   

	$count=100000;
	$var=100001;
	$startTime=microtime(1);
	for ($i=0; $i<$count; $i++) {
		mysqli_query($link, 'SELECT * FROM user;');
	}
	return round((microtime(1)-$startTime)*1000, 3);
}

function sortArr()
{
	$array = array(29, 24, 28, 27, 26, 23, 25, 41, 15, 13, 15, 15, 63, 31, 62, 12, 1, 4, 15, 4, 6 , 7, 9, 18, 35, 37, 31, 54, 52, 50);

	$count=100000;
	$var=100001;
	$startTime=microtime(1);
	for ($i=0; $i<$count; $i++) {
		quikSort($array);
	}
	return round((microtime(1)-$startTime)*1000, 3);
}

ini_set('max_execution_time', 900);
$arifTime = arifm_subsract();
$subTime = substring();
$fileTime = readWriteFile();
$DBTime = readFromDB();
$sortTime = sortArr();

function Avg(&$avg)
{
	$avg = array();
	for ($i=0; $i<9; $i++) 
	{
		$arifTime = arifm_subsract();
		$subTime = substring();
		$fileTime = readWriteFile();
		$DBTime = readFromDB();
		$sortTime = sortArr();
		$avg[$i] = $arifTime + $subTime + $fileTime + $DBTime + $sortTime;
	}

	foreach ($avg as $key => $value) {
    	$avg = $value / 9;
    }
}
$average = array();
avg($average);

echo "PHP: ".PHP_VERSION." Apache Xammpp <br>"; 
echo "Сложение 100000 раз=".$arifTime. "мс <br>";
echo "Поиск подстроки в строке 100000 раз =".$subTime . "мс <br>";
echo "Запись/чтение в файл 100000 раз =".$fileTime. "мс <br>";
echo "Чтение из БД 100000 раз =".$DBTime. "мс <br>";
echo "Быстрая сортировка 100000 раз =".$sortTime. "мс <br>";
echo "Скрипт выполнен 10 раз. <br> Среднее время выполнения скрипта =".$average."мс <br>";