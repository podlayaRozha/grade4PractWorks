<?php

class ControllerService extends Controller
{
	function __construct()
	{
		$this->model = new ModelService();
		$this->view = new View();
	}

	function actionIndex()
	{	
		$data = $this->model->getData();		
		$this->view->generate('ServiceView.php', 'templateView.php', $data);
	}

	function actionSendForm($data)
	{	
		echo $this->model->setData($data);		
	}
}