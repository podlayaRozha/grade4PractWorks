<?php

class Route
{

	static function start()
	{
		//Here we get default controller and parse string
		$controllerName = 'Main';
		$actionName = 'index';
		
		$routes = explode('/', $_SERVER['REQUEST_URI']);

		if ( !empty($routes[1]) )
		{	
			$controllerName = $routes[1];
		}
		
		if ( !empty($routes[2]) )
		{
			$actionName = $routes[2];
		}

		//Add some prefix
		$modelName = 'Model'.$controllerName;
		$controllerName = 'Controller'.$controllerName;
		$actionName = 'action'.$actionName;

		/*
		echo "Model: $modelName <br>";
		echo "Controller: $controllerName <br>";
		echo "Action: $actionName <br>";
		*/

		//Get model files. Model is not required

		$modelFile = strtolower($modelName).'.php';
		$modelPath = "application/models/".$modelFile;
		if(file_exists($modelPath))
		{
			include "application/models/".$modelFile;
		}

		//But controller is required
		$controllerFile = strtolower($controllerName).'.php';
		$controllerPath = "application/controllers/".$controllerFile;

		if(file_exists($controllerPath))
		{
			include "application/controllers/".$controllerFile;
		}
		else
		{
			Route::ErrorPage404();
		}
		
		//Create controller
		$controller = new $controllerName;
		$action = $actionName;
		
		if(method_exists($controller, $action))
		{
			//Call controller action
			$controller->$action($_POST);
		}
		else
		{
			Route::ErrorPage404();
		}
	
	}

	function ErrorPage404()
	{
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
    }
    
}
