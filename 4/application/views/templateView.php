<html>
<head>
	<title><?= substr($_SERVER['REQUEST_URI'],1);?></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script src="http://code.jquery.com/jquery-2.0.2.min.js"></script>
	<script src="main.js"></script>
	<script>

function open_dialog() {
	//$('dialog').show();
	$("#dialog").fadeIn();
}

function send_data(){
	$("#dialog").fadeOut();
    var data = $('form').serializeArray();
	$.ajax({
  	type: "POST",
  	url: "service/SendForm",
  	data: data,
  success: function(msg){
  	console.log(msg);
    if(msg == 1)
    	alert("Данные отправленные успешно");
    else alert("Ошибка при отправке данных");
  }
});
}
	</script>
</head>
<body>
	<header>
		<nav>
			<li><a href="/main">Главная</a></li>
			<li><a href="/service">Услуги</a></li>
			<li><a href="/lorem">Lorem</a></li>
			<li><a href="/lorem2">Lorem</a></li>
			<li><a href="/about">О нас</a></li>
		</nav>
	</header>
	<?=include 'application/views/'.$contentView; ?>
<footer>
		<p>Copyright here(c)</p>
	</footer>
</body>
</html>
