const http = require("http");
const fs = require("fs");
const extra = require("./extra");
const validPageLoader = require("./validPageLoader.js");

const server = http.createServer(function(req, res) {
	var url = req.url;

	filePath = extra(url);
	
	validPageLoader(filePath, res);
});
server.listen(80);