const fs = require("fs");
const validPageLoader = function(file, res) {

	fs.readFile(file, function(err, data) {
		if(err)
		{
			fs.readFile("app/404.html", function(err, data){ res.end(data);});
			return;
		}
		res.end(data);
	});
}

module.exports = validPageLoader;
