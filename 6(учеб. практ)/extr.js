let path = require("path");

let extr = function(url) {
	let filePath;
	let fileName = "index.html";
	
	if (url.length > 1) {
		fileName = url.substring(1);
	} 
	console.log(fileName);
	filePath = path.resolve(__dirname, "app", fileName);
	return filePath;
};

module.exports = extr;