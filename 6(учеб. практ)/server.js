const websocket = require("ws");

let wsserver =  websocket.Server;
const port = 3001 ;

let message = [];
let ws = new wsserver({

	port : port
});

console.log("Server Start");

ws.on("connection",function(socket) {

	console.log("Клиент подключился");
	message.forEach(function(msg) {
		socket.send(msg);
	});

	socket.on("message", function(data) {

		console.log("Приятое сообщение "+data);
		message.push(data);
		ws.clients.forEach(function(clientCosket) {
			clientCosket.send(data);
		});
		//socket.send(data);

	});
});

