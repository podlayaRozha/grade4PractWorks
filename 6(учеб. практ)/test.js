let http = require("http");
let fs = require("fs");
let extr = require ("./extr.js");
let wsserver = require("./server.js");

let server = http.createServer(function(req, res) {

	console.log("Запрос пришел");
	let url = req.url;
	let filePath = extr(url);

	fs.readFile(filePath, function(err, data) {
		if(err) {
			hanldeError(err, res);
			return;
		}
		else {
		res.end(data); 
	}
	});
});

let hanldeError = function(err, res) {
	res.writeHead(404);
	res.end();
}

server.listen(800);
