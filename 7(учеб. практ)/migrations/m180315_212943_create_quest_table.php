<?php

use yii\db\Migration;

/**
 * Handles the creation of table `quest`.
 */
class m180315_212943_create_quest_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('quest', [
            'id' => $this->primaryKey(),
            'productName' => $this->string()->notNull()->unique(),
            'description' => $this->string()->notNull(),
            'price' => $this->string()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'seller' => $this->string()->notNull(),
            'created_at' => $this->string()->notNull(),
            'updated_at' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('quest');
    }
}
