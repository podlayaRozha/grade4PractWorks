<?php

namespace app\models;

use yii\base\Model;
use app\models\User;

/**
* 
*/
class QuestForm extends Model
{	
	public $productName;
	public $description;
	public $price;
	public $quantity;
	public $seller;

	public function rules()
	{
		return[
			[['productName','description','price','quantity','seller'],'required'],
			[['productName','description','price','quantity','seller'], 'trim'],
		];
	}

	public function quest()
	{
		return;
	}
}