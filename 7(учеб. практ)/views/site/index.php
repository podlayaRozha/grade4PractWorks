<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <div id="app">
        <form v-on:submit.prevent="addQuest" >
            <p>Название<input type="text" v-model="addUserForm.productname"></p>
            <p>Описание<input type="text" v-model="addUserForm.description"></p>
            <p>Цена<input type="text" v-model="addUserForm.price" name="">
            <p>Кол-во<input type="number" min="1" v-model="addUserForm.quantity" name=""></p>
            <p>Продавец<input type="text" v-model="addUserForm.seller" name=""></p>
            <input type="submit" name="">
        </form>
        <UserTable v-bind:source.sync="users" v-bind:fields="fields"></UserTable>
    </div>
</body>
</html>